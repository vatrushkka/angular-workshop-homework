import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../posts-page/models';

@Injectable({
  providedIn: 'root'
})

export class ApiPostService {
  ROOT_URL = 'https://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) { }

  getPost(id: number) {
    const link = `${this.ROOT_URL}posts/${id}`;
    return this.http.get<Post>(link);
  }
}
