import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostPageComponent } from './post.component';

@NgModule({
  declarations: [PostPageComponent],
  imports: [CommonModule, RouterModule],
  exports: [PostPageComponent],
})
export class PostPageModule {}
