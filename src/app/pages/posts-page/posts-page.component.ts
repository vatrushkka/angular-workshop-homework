import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
// import { Post } from './models';
import { PostsService } from './services';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss'],
})
export class PostsPageComponent implements OnInit, OnDestroy {


  sub!: Subscription;

  searchForm = this.fb.group({
    query: '',
  });

  constructor(private postsService: PostsService, private fb: FormBuilder) {}

  ngOnInit(): void {
    // this.sub = this.searchForm?.valueChanges.subscribe((formValue) => {
    //   this.postsService.searchPost(formValue.query);
    //   this.data = this.postsService.getPosts();
    // });
  }

  onAddPost(newPost: { title: string; body: string }) {
    this.postsService.createPost(newPost).subscribe();
  }



  ngOnDestroy() {
    this.sub?.unsubscribe();
    this.postsService.resetPosts();
  }
}
