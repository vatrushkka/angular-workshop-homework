import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { Post } from "../../models";
import { PostsService } from "../../services";

@Component({
  selector: 'app-all-posts-page',
  templateUrl: './all-posts-page.component.html',
  styleUrls: ['./all-posts-page.component.scss']
})
export class AllPostsPageComponent implements OnInit {
  data$!: Observable<Post[]>;
  isLoading$!: Observable<boolean>;
  error$!: Observable<string>;

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    // INIT POSTS FROM SERVER
    this.postsService.getPostsApi().subscribe();

    this.data$ = this.postsService.posts$;
    this.isLoading$ = this.postsService.isLoading$;
    this.error$ = this.postsService.error$;
  }

  onDeletePost(post: Post) {
    this.postsService.deletePost(post).subscribe();
  }
}
