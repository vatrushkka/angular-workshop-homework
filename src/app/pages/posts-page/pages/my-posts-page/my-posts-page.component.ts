import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Post} from "../../models";
import {PostsService} from "../../services";

@Component({
  selector: 'app-my-posts-page',
  templateUrl: './my-posts-page.component.html',
  styleUrls: ['./my-posts-page.component.scss']
})
export class MyPostsPageComponent implements OnInit {
  data$!: Observable<Post[]>;
  isLoading$!: Observable<boolean>;
  error$!: Observable<string>;

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    // INIT POSTS FROM SERVER
    this.postsService.getMyPostsApi(1).subscribe();

    this.data$ = this.postsService.posts$;
    this.isLoading$ = this.postsService.isLoading$;
    this.error$ = this.postsService.error$;
  }

  onDeletePost(post: Post) {
    this.postsService.deletePost(post).subscribe();
  }
}
