import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostPageComponent } from "../post-page/post.component";
import { AllPostsPageComponent } from "./pages/all-posts-page/all-posts-page.component";
import { MyPostsPageComponent } from "./pages/my-posts-page/my-posts-page.component";
import { PostsPageComponent } from "./posts-page.component";

const routes: Routes = [
  {
    path: '',
    component: PostsPageComponent,
    children: [
      { path: '', redirectTo: 'all-posts', pathMatch: 'full' },
      { path: 'all-posts', component: AllPostsPageComponent },
      { path: 'my-posts', component: MyPostsPageComponent }
    ]
  },
  { path: 'post/:id', component: PostPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsPageRoutingModule { }
